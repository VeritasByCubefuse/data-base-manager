# app.py

# Required imports
import os
from flask import Flask, request, jsonify
from firebase_admin import credentials, firestore, initialize_app



# Initialize Flask app
app = Flask(__name__)


# Initialize Firestore DB
cred = credentials.Certificate('key.json')
default_app = initialize_app(cred)
db = firestore.client()
todo_ref = db.collection('twitter_data')




@app.route('/')
def healthcheck():
    return 'ok'



@app.route('/white', methods=['POST'])
def addTrue():
    """
        create() : Add document to Firestore collection with request body.
        Ensure you pass a custom ID as part of json body in post request,
        e.g. json={'id': '1', 'title': 'Write a blog post'}
    """
    parsed_tweet = {}

    try:
        id = request.json['id_str']

        parsed_tweet["id"] = request.json["id"]
        
        # Style
        parsed_tweet["text"] = request.json["text"]
        parsed_tweet["num_mentions"] = len(request.json["entities"]["user_mentions"])
        parsed_tweet["num_hashtags"] = len(request.json["entities"]["hashtags"])
        parsed_tweet["num_urls"] = len(request.json["entities"]["urls"])
        
        if "media" in request.json["entities"].keys():
            parsed_tweet["has_media"] = True
        else:
            parsed_tweet["has_media"] = False
        
        # Propagation
        parsed_tweet["num_likes"] = request.json["favorite_count"]
        parsed_tweet["num_retweets"] = request.json["retweet_count"]
        
        # Credibility
        parsed_tweet["user_verified"] = request.json["user"]["verified"]
        parsed_tweet["user_no_profile_image"] = request.json["user"]["default_profile_image"]
        parsed_tweet["user_num_friends"] = request.json["user"]["friends_count"]
        parsed_tweet["user_num_followers"] = request.json["user"]["followers_count"]
        parsed_tweet["user_num_lists"] = request.json["user"]["listed_count"]
        parsed_tweet["user_num_tweets"] = request.json["user"]["statuses_count"]
        parsed_tweet["user_num_friends"] = request.json["user"]["friends_count"]
        parsed_tweet["user_num_favourite_tweets"] = request.json["user"]["favourites_count"]
        parsed_tweet["user_protected"] = request.json["user"]["protected"]
        
        if request.json["coordinates"] != None:
            parsed_tweet["has_location"] = True
        else:
            parsed_tweet["has_location"] = False


        parsed_tweet["annotation"] = True
        
        # For filtering
        if request.json["lang"] != "en":
            return jsonify({"status": "rejected lang is not English"}),200
        else:
            parsed_tweet["language"] = request.json["lang"]


            todo_ref.document(id).set(parsed_tweet)
            return jsonify({"success": True}), 200
    except Exception as e:
        return f"An Error Occured: {e}"



@app.route('/black', methods=['POST'])
def addFake():
    """
        create() : Add document to Firestore collection with request body.
        Ensure you pass a custom ID as part of json body in post request,
        e.g. json={'id': '1', 'title': 'Write a blog post'}
    """
    parsed_tweet = {}

    try:
        id = request.json['id_str']

        parsed_tweet["id"] = request.json["id"]
        
        # Style
        parsed_tweet["text"] = request.json["text"]
        parsed_tweet["num_mentions"] = len(request.json["entities"]["user_mentions"])
        parsed_tweet["num_hashtags"] = len(request.json["entities"]["hashtags"])
        parsed_tweet["num_urls"] = len(request.json["entities"]["urls"])
        
        if "media" in request.json["entities"].keys():
            parsed_tweet["has_media"] = True
        else:
            parsed_tweet["has_media"] = False
        
        # Propagation
        parsed_tweet["num_likes"] = request.json["favorite_count"]
        parsed_tweet["num_retweets"] = request.json["retweet_count"]
        
        # Credibility
        parsed_tweet["user_verified"] = request.json["user"]["verified"]
        parsed_tweet["user_no_profile_image"] = request.json["user"]["default_profile_image"]
        parsed_tweet["user_num_friends"] = request.json["user"]["friends_count"]
        parsed_tweet["user_num_followers"] = request.json["user"]["followers_count"]
        parsed_tweet["user_num_lists"] = request.json["user"]["listed_count"]
        parsed_tweet["user_num_tweets"] = request.json["user"]["statuses_count"]
        parsed_tweet["user_num_friends"] = request.json["user"]["friends_count"]
        parsed_tweet["user_num_favourite_tweets"] = request.json["user"]["favourites_count"]
        parsed_tweet["user_protected"] = request.json["user"]["protected"]
        
        if request.json["coordinates"] != None:
            parsed_tweet["has_location"] = True
        else:
            parsed_tweet["has_location"] = False


        parsed_tweet["annotation"] = True

        
        # For filtering
        if request.json["lang"] != "en":
            return jsonify({"status": "rejected lang is not English"}),200
        else:
            parsed_tweet["language"] = request.json["lang"]


            todo_ref.document(id).set(parsed_tweet)
            return jsonify({"success": True}), 200
    except Exception as e:
        return f"An Error Occured: {e}"




@app.route('/list', methods=['GET'])
def read():
    """
        read() : Fetches documents from Firestore collection as JSON.
        todo : Return document that matches query ID.
        all_todos : Return all documents.
    """
    try:
        # Check if ID was passed to URL query
        todo_id = request.args.get('id')
        if todo_id:
            todo = todo_ref.document(todo_id).get()
            return jsonify(todo.to_dict()), 200
        else:
            all_todos = [doc.to_dict() for doc in todo_ref.stream()]
            return jsonify(all_todos), 200
    except Exception as e:
        return f"An Error Occured: {e}"

@app.route('/update', methods=['POST', 'PUT'])
def update():
    """
        update() : Update document in Firestore collection with request body.
        Ensure you pass a custom ID as part of json body in post request,
        e.g. json={'id': '1', 'title': 'Write a blog post today'}
    """
    try:
        id = request.json['id']
        todo_ref.document(id).update(request.json)
        return jsonify({"success": True}), 200
    except Exception as e:
        return f"An Error Occured: {e}"

@app.route('/delete', methods=['GET', 'DELETE'])
def delete():
    """
        delete() : Delete a document from Firestore collection.
    """
    try:
        # Check for ID in URL query
        todo_id = request.args.get('id')
        todo_ref.document(todo_id).delete()
        return jsonify({"success": True}), 200
    except Exception as e:
        return f"An Error Occured: {e}"

port = int(os.environ.get('PORT', 5000))
if __name__ == '__main__':
    app.run(threaded=True, host='0.0.0.0', port=port)
